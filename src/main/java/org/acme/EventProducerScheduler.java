package org.acme;

import io.quarkus.scheduler.Scheduled;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;

@ApplicationScoped
public class EventProducerScheduler {

    @Inject
    EventProducer producer;

    @Scheduled(every = "10s")
    void sendMessage() {
        producer.produceEvent(CreditCard
                        .newBuilder()
                        //.setOwnerName("Hamed Hatami")
                        .setCardNumber("1111-2222-3333-4444")
                        .setExpirationYear(1978)
                        .setExpirationMonth(3)
                        .setSecurityCode(200)
                        .setAvailableCredit(60000)
                        .build())
                .subscribe()
                .with(success -> System.out.println("Succeed to produce"),
                        fail -> System.out.println("fail to produce"));
    }
}
