package org.acme;

import org.eclipse.microprofile.reactive.messaging.Channel;

import io.smallrye.mutiny.Uni;
import io.smallrye.reactive.messaging.MutinyEmitter;
import jakarta.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class EventProducer {

    @Channel("event-out")
    MutinyEmitter<CreditCard> emitter;

    public Uni<Void> produceEvent(final CreditCard creditCardDto) {
        return emitter.send(creditCardDto).replaceWithNull();
    }


}