package org.acme;

import org.eclipse.microprofile.reactive.messaging.Incoming;
import org.jboss.logging.Logger;

import jakarta.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class EventConsumer {

    static final Logger LOGGER = Logger.getLogger("EventConsumer");

    @Incoming("event-in")
    public void consumeEvent(final CreditCard creditCardDto) {
        LOGGER.infof("The message '%s'", creditCardDto.getOwnerName());
    }
}
