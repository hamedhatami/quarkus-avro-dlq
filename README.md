# Quarkus Avro DLQ

This application is to create a reproducer for getting DLQ qorking for kafka producer when there's a failure in avro matching

In this application after startup a scheduler will start producing messages towards kafka topic with mismatch format so the expectation is this message goes to DLQ

## local kafka up and running

     docker-compose -f local-kafka.yaml up -d

